package com.silvia18.misiontic.ventasdomicilio;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

public class LoginActivity extends AppCompatActivity {

    AppCompatButton btnLogin;
    AppCompatButton btnFacebook;
    AppCompatButton btnGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> onLoginClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(v -> onFacebookClick());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> onGoogleClick());
    }

    private void onLoginClick() {
        Intent intent= new Intent( LoginActivity.this, PaymentsActivity.class);
        startActivity(intent);
    }

    private void onFacebookClick() {
        Intent intent= new Intent( LoginActivity.this, PaymentsActivity.class);
        startActivity(intent);
    }

    private void onGoogleClick() {
        Intent intent= new Intent( LoginActivity.this, PaymentsActivity.class);
        startActivity(intent);
    }
}