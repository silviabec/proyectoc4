package com.silvia18.misiontic.ventasdomicilio;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class NewSaleActivity extends AppCompatActivity {

    private TextInputLayout tilClient;
    private TextInputEditText etClient;
    private TextInputLayout tilAddress;
    private TextInputEditText etAddress;
    private TextInputLayout tilAmount;
    private TextInputEditText etAmount;
    private TextInputLayout tilNumber;
    private TextInputEditText etNumber;
    private TextInputLayout tilPeriodicity;
    private TextInputEditText etPeriodicity;
    private MaterialAutoCompleteTextView et_periodicity;
    private TextInputLayout tilPart;
    private TextInputEditText etPart;
    private TextInputLayout tilDate;
    private TextInputEditText etDate;
    private AppCompatButton btnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sale);

        initUI();
    }

    private void initUI() {

        tilDate= findViewById(R.id.til_date);

        etDate = findViewById(R.id.et_date);
    }

    private void onDateClick() {
        MaterialDatePicker datePicker = MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Select date")
                    .build();
}
}